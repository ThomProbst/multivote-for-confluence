package com.tngtech.confluence.plugin;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItems;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import com.atlassian.confluence.cluster.ClusterManager;
import com.atlassian.confluence.cluster.ClusteredLock;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.user.UserAccessor;
import com.google.gson.Gson;
import com.tngtech.confluence.plugin.data.AudienceData;
import com.tngtech.confluence.plugin.data.ItemKey;

public class DefaultMultiVoteTest {
    private DefaultMultiVoteService multivote;
    private ClusterManager clusterManager;
    private ContentPropertyManager contentPropertyManager;
    private UserAccessor userAccessor;
    private String user = "user";
    private ContentEntityObject page = mock(ContentEntityObject.class);
    private String tableId = "tableId";
    private String itemId = "itemId";
    private String key = "multivote.tableId.itemId";
    private ClusteredLock lock;

    private ItemKey itemKey;

    @Before
    public void setUp() throws Exception {
        multivote = new DefaultMultiVoteService();
        clusterManager = mock(ClusterManager.class);
        lock = mock(ClusteredLock.class);
        when(clusterManager.getClusteredLock(anyString())).thenReturn(lock);
        contentPropertyManager = mock(ContentPropertyManager.class);
        userAccessor = mock(UserAccessor.class);

        multivote.setClusterManager(clusterManager);
        multivote.setContentPropertyManager(contentPropertyManager);
        multivote.setUserAccessor(userAccessor);

        itemKey = new ItemKey(page, tableId, itemId);
    }

    @Test
    public void test_voting_locks() {
        multivote.recordInterest(user, true, itemKey);
        InOrder inOrder = inOrder(lock, contentPropertyManager, lock);

        String json = getAudienceDataAsJsonFromUsers(user);

        inOrder.verify(lock).lock();
        inOrder.verify(contentPropertyManager).setTextProperty(page, key, json);
        inOrder.verify(lock).unlock();
    }

    @Test
    public void test_voting_for_item_persists_audience() {
        multivote.recordInterest(user, true, itemKey);

        String json = getAudienceDataAsJsonFromUsers(user);

        verify(contentPropertyManager).setTextProperty(page, key, json);
    }

    @Test
    public void test_voting_when_already_voted_does_not_persist() {

        String json = getAudienceDataAsJsonFromUsers("otherUser1", "user", "otherUser2");

        when(contentPropertyManager.getTextProperty(page, key)).thenReturn(json);

        multivote.recordInterest(user, true, itemKey);

        verify(contentPropertyManager, never()).setTextProperty(eq(page), eq("multivote.tableId.itemId"),
                anyString());
    }


    @Test
    public void test_voting_against_when_voted_before_does_persist() {

        String json = getAudienceDataAsJsonFromUsers("otherUser1", "user", "otherUser2");

        when(contentPropertyManager.getTextProperty(page, key)).thenReturn(json);

        multivote.recordInterest(user, false, itemKey);

        verify(contentPropertyManager).setTextProperty(page, key, getAudienceDataAsJsonFromUsers("otherUser1", "otherUser2"));
    }

    @Test
    public void test_voting_against_item_that_was_not_voted_for_does_not_persist() {
        multivote.recordInterest(user, false, itemKey);

        verify(contentPropertyManager, never()).setTextProperty(page, "multivote.tableId.itemId", "");
    }

    @Test
    public void test_retrieveAudience_empty() {
        Set<AudienceData> audience = multivote.retrieveAudience(itemKey);
        verify(contentPropertyManager).getTextProperty(page, key);
        assertThat(audience, hasSize(0));
    }

    @Test
    public void test_retrieveAudience_users() {
        when(contentPropertyManager.getTextProperty(page, key)).thenReturn(getAudienceDataAsJsonFromUsers("user1", "user2"));
        Set<AudienceData> audience = multivote.retrieveAudience(itemKey);
        assertThat(audience, hasSize(2));
        assertThat(audience, hasItems(new AudienceData("user1"), new AudienceData("user2")));
    }

    private String getAudienceDataAsJsonFromUsers(String... users) {
        Set<AudienceData> audience = new TreeSet<AudienceData>();

        for (String user : users) {
            audience.add(new AudienceData(user));
        }

        Gson gson = new Gson();
        return gson.toJson(audience);
    }
}
