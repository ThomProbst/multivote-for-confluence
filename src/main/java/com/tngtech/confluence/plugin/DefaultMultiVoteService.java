package com.tngtech.confluence.plugin;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.atlassian.confluence.setup.settings.SettingsManager;
import org.apache.commons.lang.StringUtils;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheFactory;
import com.atlassian.confluence.cluster.ClusterManager;
import com.atlassian.confluence.cluster.ClusteredLock;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tngtech.confluence.plugin.data.AudienceData;
import com.tngtech.confluence.plugin.data.ItemKey;
import com.tngtech.confluence.plugin.data.VoteItem;

public class DefaultMultiVoteService implements MultiVoteService {

    public VoteItem recordInterest(String remoteUser, boolean requestUse, ItemKey key) {
        ClusteredLock lock = getLock(key);
        Set<AudienceData> users;
        try {
            lock.lock();
            users = doRecordInterest(remoteUser, requestUse, key);
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
        return new VoteItem(key.getItemId(), users);
    }

    @Override
    public void reset(ContentEntityObject page, String tableId, List<String> itemIds) {
        for (String itemId : itemIds) {
            ItemKey key = new ItemKey(page, tableId, itemId);

            ClusteredLock lock = getLock(key);

            try {
                lock.lock();
                String property = buildPropertyString(key);
                contentPropertyManager.removeProperty(key.getPage(), property);
            } finally {
                if (lock != null) {
                    lock.unlock();
                }
            }
        }
    }

    private ClusteredLock getLock(ItemKey key) {
        return clusterManager.getClusteredLock("multivote.lock." + key.getTableId() + "." + key.getItemId());
    }

    private Set<AudienceData> doRecordInterest(String user, Boolean requestUse, ItemKey key) {
        boolean changed;
        Set<AudienceData> users = retrieveAudience(key);
        AudienceData newUser = new AudienceData(user);

        if (requestUse) {
            changed = users.add(newUser);
        } else {
            changed = users.remove(newUser);
        }
        if (changed) {
            persistAudience(key, users);
        }
        return users;
    }

    public Set<AudienceData> retrieveAudience(ItemKey key) {
        String usersAsString = contentPropertyManager
                .getTextProperty(key.getPage(), buildPropertyString(key));
        if (usersAsString == null) {
            usersAsString = "";
        }

        Gson gson = new Gson();
        Type collectionType = new TypeToken<Collection<AudienceData>>() {
        }.getType();
        Collection<AudienceData> audienceData = gson.fromJson(usersAsString, collectionType);

        Set<AudienceData> audienceDataSet;
        if (audienceData == null) {
            audienceDataSet = new TreeSet<AudienceData>();
        }
        else {
            audienceDataSet = new TreeSet<AudienceData>(audienceData);
        }

        return audienceDataSet;
    }

    private void persistAudience(ItemKey key, Collection<AudienceData> audienceData) {
        String property = buildPropertyString(key);

        Gson gson = new Gson();
        String propertyValue = gson.toJson(audienceData);

        contentPropertyManager.setTextProperty(key.getPage(), property, propertyValue);
    }

    private String buildPropertyString(ItemKey key) {
        return "multivote." + key.getTableId() + "." + key.getItemId();
    }

    @Override
    public String getUserFullNamesAsString(Set<AudienceData> audience) {
        List<String> fullNames = new ArrayList<String>();

        for (AudienceData voter : audience) {
            fullNames.add(getFullName(voter));
        }
        return StringUtils.join(fullNames, ", ");
    }

    @HtmlSafe
    @Override
    public String getUserFullNamesAsHtml(Set<AudienceData> audience, PageContext context)
            throws MacroException {
        List<String> userLinks = new ArrayList<String>(audience.size());

        for (AudienceData voter : audience) {

            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");

            StringBuilder voterAndDate = new StringBuilder();
            voterAndDate.append(getUserLink(voter.getVoter(), context));
            voterAndDate.append(" (").append(format.format(voter.getVotedAt())).append(")");

            userLinks.add(voterAndDate.toString());
        }

        return StringUtils.join(userLinks, ", "); // TODO configurable
    }

    private String getUserLink(String userName, PageContext context) throws MacroException {
        final Cache cache = cacheFactory.getCache("com.tngtech.confluence.plugin.multivote.userLink");
        String result = (String) cache.get(userName);
        if (result == null) {
            Map<String, Object> contextMap = new HashMap<String, Object>();
            contextMap.put("userName", userName);

            try {
                result = xmlXhtmlContent.convertStorageToView(
                        VelocityUtils.getRenderedTemplate("templates/extra/userlink.vm", contextMap),
                        new DefaultConversionContext(context)
                        );
            } catch (Exception e) {
                throw new MacroException(e);
            }

            cache.put(userName, result);
        }

        return result;
    }

    private String getFullName(AudienceData voter) {
        String fullName = voter.getVoter();
        User user = userAccessor.getUser(fullName);
        if (user != null) {
            fullName = user.getFullName();
        }

        return fullName;
    }

    /*
     * injected Services
     */
    private ContentPropertyManager contentPropertyManager;
    private UserAccessor userAccessor;
    private ClusterManager clusterManager;
    private XhtmlContent xmlXhtmlContent;
    private CacheFactory cacheFactory;

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = contentPropertyManager;


    }

    public void setUserAccessor(UserAccessor userAccessor) {
        this.userAccessor = userAccessor;
    }

    public void setClusterManager(ClusterManager clusterManager) {
        this.clusterManager = clusterManager;
    }

    public void setXmlXhtmlContent(XhtmlContent xmlXhtmlContent) {
        this.xmlXhtmlContent = xmlXhtmlContent;
    }

    public void setCacheFactory(CacheFactory cacheFactory) {
        this.cacheFactory = cacheFactory;
    }
}
