/*
 * Copyright (C) Schweizerische Bundesbahnen SBB, 2014.
 */

package com.tngtech.confluence.plugin.data;

import java.util.Date;

public class AudienceData implements Comparable {

    private String voter;

    private Date votedAt;

    public AudienceData() {
    }

    public AudienceData(String voter) {
        this.voter = voter;
        this.votedAt = new Date();
    }

    public String getVoter() {
        return voter;
    }

    public void setVoter(String voter) {
        this.voter = voter;
    }

    public Date getVotedAt() {
        return votedAt;
    }

    public void setVotedAt(Date votedAt) {
        this.votedAt = votedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        AudienceData that = (AudienceData) o;

        if (!voter.equals(that.voter))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return voter.hashCode();
    }

    @Override
    public int compareTo(Object o) {

        if (this == o)
            return 0;
        if (o == null || getClass() != o.getClass())
            return -1;

        AudienceData that = (AudienceData) o;

        return voter.compareTo(that.voter);
    }
}
