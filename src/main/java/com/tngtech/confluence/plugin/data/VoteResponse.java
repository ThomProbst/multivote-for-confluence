package com.tngtech.confluence.plugin.data;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "interested")
@XmlAccessorType(XmlAccessType.FIELD)
public class VoteResponse {
    @XmlElement(name = "id")
    private String id;
    @XmlAttribute
    private String users;
    @XmlAttribute
    private String htmlUsers;
    @XmlAttribute
    private int userNo;

    public VoteResponse() {

    }
   
    public VoteResponse(String id, String users, String htmlUsers, int userNo) {
        this.id = id;
        this.users = users;
        this.userNo = userNo;
        this.htmlUsers = htmlUsers;
    }

    public String getId() {
        return id;
    }

    public String getUsers() {
        return users;
    }

    public String htmlUsers() {
        return htmlUsers;
    }

    public int getUserNo() {
        return userNo;
    }
}
